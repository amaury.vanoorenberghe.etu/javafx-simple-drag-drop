package app;

import javafx.geometry.BoundingBox;
import javafx.geometry.Point2D;

/** Collisions rectangulaires avec des fonctions en plus acceptant des Point2D **/
public class RectangleBounds extends BoundingBox {
    public RectangleBounds(double minX, double minY, double width, double height) {
        super(minX, minY, width, height);
    }

    public RectangleBounds(Point2D center, Point2D size) {
        this(center.getX() - size.getX() / 2, center.getY() - size.getY() / 2, size.getX(), size.getY());
    }

    public Point2D getCorner() {
        return new Point2D(getMinX(), getMinY());
    }

    public Point2D getCenter() {
        return new Point2D(0.5 * (getMinX() + getMaxX()), 0.5 * (getMinY() + getMaxY()));
    }

    public Point2D getSize() {
        return new Point2D(getWidth(), getHeight());
    }
}
