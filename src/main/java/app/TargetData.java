package app;

import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.ReadOnlyObjectWrapper;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.paint.Color;

/** Type de données contenues dans un DragTarget **/
public class TargetData {
    private SimpleIntegerProperty number;
    private SimpleBooleanProperty active;

    public TargetData(int number, boolean active) {
        this.number = new SimpleIntegerProperty(number);
        this.active = new SimpleBooleanProperty(active);
    }

    public SimpleIntegerProperty numberProperty() {
        return number;
    }

    public SimpleBooleanProperty activeProperty() {
        return active;
    }

    public ReadOnlyObjectProperty<Color> colorProperty() {
        Color color = active.get() ? Color.YELLOWGREEN : Color.RED;
        
        if (numberProperty().get() == 0) {
        	color = color.darker().desaturate();
        }
        
        return new ReadOnlyObjectWrapper<Color>(color);
    }
}
