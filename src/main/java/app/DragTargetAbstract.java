package app;

import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;

/** Cible de d'ag and drop abstraite (dont le contenu est de type T) **/
public abstract class DragTargetAbstract<T> {
    private RectangleBounds bounds;
    private SimpleObjectProperty<T> content;

    protected static final double DEFAULT_SIZE_1D = 50;
    protected static final Point2D DEFAULT_SIZE_2D = new Point2D(DEFAULT_SIZE_1D, DEFAULT_SIZE_1D);

    public DragTargetAbstract(Point2D center, Point2D size, T content) {
        bounds = new RectangleBounds(center, size);
        this.content = new SimpleObjectProperty<T>(content);
    }

    public DragTargetAbstract(Point2D center, double size, T content) {
        this(center, new Point2D(size, size), content);
    }

    public DragTargetAbstract(Point2D center, T content) {
        this(center, DEFAULT_SIZE_2D, content);
    }

    public DragTargetAbstract(double x, double y, T content) {
        this(new Point2D(x, y), content);
    }

    public DragTargetAbstract(double x, double y, double size, T content) {
        this(new Point2D(x, y), size, content);
    }

    /** Obtient la zone 2D qu'occupe l'élément **/
    public RectangleBounds getBounds() {
        return bounds;
    }

    /** Obtient la propriété de contenu **/
    public SimpleObjectProperty<T> contentProperty() {
        return content;
    }

    /** Dessiner l'élément sur un GraphicsContext existant **/
    public abstract void draw(GraphicsContext gc);
}
