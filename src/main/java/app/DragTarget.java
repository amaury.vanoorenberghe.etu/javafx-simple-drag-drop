package app;

import javafx.geometry.Point2D;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;

public class DragTarget extends DragTargetAbstract<TargetData> {
    public DragTarget(Point2D center, Point2D size, TargetData content) {
        super(center, size, content);
    }

    public DragTarget(Point2D center, double size, TargetData content) {
        super(center, size, content);
    }

    public DragTarget(Point2D center, TargetData content) {
        super(center, content);
    }

    public DragTarget(double x, double y, TargetData content) {
        super(x, y, content);
    }

    public DragTarget(double x, double y, double size, TargetData content) {
        super(x, y, size, content);
    }

    @Override
    public void draw(GraphicsContext gc) {
        int number = contentProperty().get().numberProperty().get();
        boolean active = contentProperty().get().activeProperty().get();
        Color color = contentProperty().get().colorProperty().get();

        gc.setFill(color);

        Point2D pos = getBounds().getCorner();
        Point2D size = getBounds().getSize();

        gc.fillRect(pos.getX(), pos.getY(), size.getX(), size.getY());

        gc.setTextAlign(TextAlignment.CENTER);

        gc.setFont(new Font(20));
        gc.setFill(Color.WHITE);
        gc.fillText(Integer.toString(number), getBounds().getCenter().getX(), getBounds().getCenter().getY() + 5);
    }
}
