package app;

import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Point2D;
import javafx.scene.Cursor;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.TextAlignment;
import javafx.scene.transform.Affine;
import javafx.scene.transform.NonInvertibleTransformException;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

/** Surface de dessin principale de l'application **/
public class DragDropCanvas extends Canvas {
    private List<DragTarget> targets;

    private DragTarget selected = null;
    private DragTarget target = null;
    private SimpleObjectProperty<Point2D> cursor = new SimpleObjectProperty<Point2D>();

    public DragDropCanvas() {
        super(500, 500);

        targets = new ArrayList<DragTarget>();
        cursor.set(Point2D.ZERO);

        Random rng = new Random();

        final int SIZE = 2;

        for (int x = -SIZE; x <= SIZE; ++x) {
            for (int y = -SIZE; y <= SIZE; ++y) {
                TargetData data = new TargetData(rng.nextInt(10), rng.nextBoolean());
                targets.add(new DragTarget(60 * (x - 1), 60 * y, data));
            }
        }

        targets.add(new DragTarget(150, 30, 110, new TargetData(rng.nextInt(10), rng.nextBoolean())));
        targets.add(new DragTarget(150, -90, 110, new TargetData(rng.nextInt(10), rng.nextBoolean())));

        setOnMousePressed(this::onMousePressed);
        setOnMouseDragged(this::onMouseDragged);
        setOnMouseReleased(this::onMouseReleased);
        setOnMouseMoved(this::onMouseMoved);

        draw();
    }

    private void onMouseMoved(MouseEvent e) {
        cursor.set(getCursor(e));

        if (selected == null) {
            Cursor pointer = Cursor.DEFAULT;

            for (DragTarget t : targets) {
                if (t.getBounds().contains(cursor.get()) && dragAllowed(t)) {
                    pointer = Cursor.CLOSED_HAND;
                    break;
                }
            }

            getScene().setCursor(pointer);
        }
    }

    private void onMousePressed(MouseEvent e) {
        cursor.set(getCursor(e));

        switch (e.getButton()) {
            case PRIMARY:
                for (DragTarget t : targets) {
                    if (t.getBounds().contains(cursor.get()) && dragAllowed(t)) {
                        selected = t;
                        break;
                    }
                }
                break;
            case SECONDARY:
            	if (selected != null) break;
            	
            	for (DragTarget t : targets) {
                    if (t.getBounds().contains(cursor.get())) {
                    	var prop = t.contentProperty().get().activeProperty();
                    	prop.set(!prop.get());
                    	break;
                    }
                }
            	
            	draw();
            	break;
        }
    }

    private void onMouseDragged(MouseEvent e) {
        cursor.set(getCursor(e));

        switch (e.getButton()) {
            case PRIMARY:
            	
            	if (selected != null) {
	            	target = null;
	
	                for (DragTarget t : targets) {
	                    if (t.getBounds().contains(cursor.get()) && dropAllowed(selected, t)) {
	                        target = t;
	                        break;
	                    }
	                }
                
                    //getScene().setCursor(Cursor.CLOSED_HAND);
                    getScene().setCursor(Cursor.NONE);
                }

                break;
        }
        
        draw();
    }

    private void onMouseReleased(MouseEvent e) {
        cursor.set(getCursor(e));

        switch (e.getButton()) {
	        case PRIMARY:
	        	 if (selected != null) {
		            getScene().setCursor(Cursor.DEFAULT);
		
		            if (target != null) {
		                TargetData temp = selected.contentProperty().get();
		                selected.contentProperty().set(target.contentProperty().get());
		                target.contentProperty().set(temp);
		            }
		
		            selected = null;
		            target = null;
		            
		            draw();
		        } else {
		            getScene().setCursor(Cursor.CLOSED_HAND);
		        }
	        	break;
        }   
    }

    private void draw() {
        GraphicsContext gc = getGraphicsContext2D();

        // Remettre la caméra à ZERO
        gc.setTransform(new Affine());

        // Vider l'écran
        gc.setFill(Color.LIGHTGRAY);
        gc.fillRect(0, 0, getWidth(), getHeight());


        // Placer le point 0;0 au centre du canvas
        gc.translate(getWidth() / 2, getHeight() / 2);

        // Dessiner chaque élément d'interface
        for (DragTargetAbstract t : targets) {
            t.draw(gc);
        }

        // Si un élément est sélectionné
        if (selected != null) {
            Point2D size = selected.getBounds().getSize();

            Color color = selected.contentProperty().get().colorProperty().get();
            gc.setFill(color.interpolate(Color.TRANSPARENT, 0.25));

            gc.fillRect(cursor.get().getX() - size.getX() / 2, cursor.get().getY() - size.getY() / 2, size.getX(), size.getY());

            gc.setStroke(Color.WHITE);
            gc.setFill(Color.WHITE);

            gc.setLineWidth(2);
            gc.setLineDashes(5);
            gc.setLineDashOffset(2);
            gc.setFont(new Font(20));


            gc.strokeRect(cursor.get().getX() - size.getX() / 2, cursor.get().getY() - size.getY() / 2, size.getX(), size.getY());

            gc.setTextAlign(TextAlignment.CENTER);

            int number = selected.contentProperty().get().numberProperty().get();

            gc.fillText(Integer.toString(number), cursor.get().getX(), cursor.get().getY() + 5);
        }
        
        if (target != null && !target.equals(selected)) {
        	Color hilight = target.contentProperty().get().colorProperty().get().desaturate().brighter().saturate().brighter().saturate().saturate();
        	gc.setFill(hilight.interpolate(Color.TRANSPARENT, 0.8));
        	gc.setStroke(hilight.interpolate(Color.TRANSPARENT, 0.1));
        	gc.setLineWidth(2);
        	
        	
        	Point2D size = target.getBounds().getSize();
        	Point2D pos = target.getBounds().getCorner();
        	
        	gc.fillRect(pos.getX(), pos.getY(), size.getX(), size.getY());
        	gc.strokeRect(pos.getX(), pos.getY(), size.getX(), size.getY());
        }
    }

    // lire la position du curseur
    private Point2D getCursor(MouseEvent e) {
        GraphicsContext gc = getGraphicsContext2D();
        Point2D cursor = new Point2D(e.getX(), e.getY());

        try {
            // Convertir les coordonnées sur l'écran en coordonnées de la caméra du canvas
            cursor = gc.getTransform().inverseTransform(cursor);
        } catch (NonInvertibleTransformException ex) {
            ex.printStackTrace();
        }

        return cursor;
    }
    
    private boolean dragAllowed(DragTarget source) {
    	TargetData srcDat = source.contentProperty().get();

    	return srcDat.numberProperty().get() != 0;
    }
    
    private boolean dropAllowed(DragTarget source, DragTarget destination) {
    	TargetData srcDat = source.contentProperty().get();
    	TargetData dstDat = destination.contentProperty().get();
    	
    	return srcDat.numberProperty().get() >= dstDat.numberProperty().get();
    }
}