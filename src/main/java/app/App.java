package app;

import javafx.application.Application;
import javafx.beans.binding.Bindings;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.Pane;
import javafx.scene.layout.TilePane;
import javafx.stage.Stage;


/**
 * JavaFX App
 */
public class App extends Application {
    @Override
    public void start(Stage stage) {
    	TilePane root = new TilePane();
    	DragDropCanvas canvas = new DragDropCanvas();
    	
    	root.setAlignment(Pos.CENTER);
    	root.getChildren().setAll(canvas);
    	
    	root.prefWidthProperty().bind(canvas.widthProperty());
    	root.prefHeightProperty().bind(canvas.heightProperty());
    	
        Scene scene = new Scene(root);
        
        stage.setScene(scene);
        stage.setResizable(true);
        stage.setTitle("Glisser-déposer");
        stage.show();
    }

    public static void main(String[] args) {
        launch();
    }
}